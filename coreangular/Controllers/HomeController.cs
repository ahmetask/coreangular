using Microsoft.AspNetCore.Mvc;

namespace coreangular.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}