﻿using System.Collections.Generic;
using System.Linq;
using coreangular.EF;
using coreangular.Model;
using coreangular.Models;
using coreangular.Models.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace coreangular.Controllers
{
    public class CargoFirmController : Controller
    {
      

        [HttpGet]
        public IActionResult GetCargoFirm(int id)
        {
            using (var dbContext = new DBContext())
            {
                if (id == 0)
                {
                    var cargoFirmList = dbContext.CargoFirm.AsNoTracking().ToList();
                 
                    return Json(cargoFirmList);

                }
                else
                {
                    var cargoFirmList = dbContext.CargoFirm.AsNoTracking().SingleOrDefault(a => a.ID.Equals(id));

                    return Json(cargoFirmList);
                }

            }
        }

        [HttpGet]
        public IActionResult GetCities(int id)
        {
            using (var dbContext = new DBContext())
            {
                if (id == 0)
                {
                    var error = new ErrorDTO();
                    error.message = "empty city";
                    error.statuscode = 400;
                    return Json(error);
                }
             
                else
                {
                    var citiesen =( from ct in dbContext.FirmCountry where ct.CountryID == id select ct.CityID).Distinct();
                    List<City> cities = new List<City>();
                    foreach (var cityid in citiesen.ToList())
                    {
                        var City = dbContext.City.AsNoTracking().SingleOrDefault(a => a.ID == cityid);
                 
                        cities.Add(City);
                    }
                    return Json(cities);

                }
            }
        }
        [HttpGet]
        public IActionResult GetCountry(int id)
        {
            using (var dbContext = new DBContext())
            {
                if (id == 0)
                {

                    var error = new ErrorDTO();
                    error.message = "empty country";
                    error.statuscode = 400;
                    return Json(error);

                }
                else
                {

                    var cargoFirmCountry = (from ct in dbContext.FirmCountry where ct.FirmID == id select  ct.CountryID).Distinct();
                    List<Country> countries = new List<Country>();
                    foreach (var countryid in cargoFirmCountry.ToList())
                    {
                        var Country = dbContext.Country.AsNoTracking().SingleOrDefault(a => a.ID ==countryid);
                        countries.Add(Country);
                    }
                    return Json(countries);
                }

            }
        }
     
        [HttpPost]
        public IActionResult CreateCargoFirm([FromBody] CargoFirm cargoFirm)
        {
            if (cargoFirm == null )
                return BadRequest();
            else
            {
                using (var dbContext = new DBContext())
                {   

                    
                    var newFirm = dbContext.CargoFirm.Add(cargoFirm);
                   
                    
                    dbContext.SaveChanges();

                    return Json(newFirm.Entity);
                }
            }
        }
        /*
        [HttpDelete]
        public IActionResult DeleteCargoFirm(int id)
        {
            using (var dbContext = new DBContext())
            {
                 var cargoFirm = dbContext.CargoFirm.AsNoTracking().SingleOrDefault(a => a.ID.Equals(id));
                if(cargoFirm!=null)
                    dbContext.CargoFirm.Remove(cargoFirm);
                dbContext.SaveChanges();

                return Json(cargoFirm);
           
                }

        }*/

     
        [HttpPut]
        public IActionResult ChangeCargoFirm([FromBody] CargoFirm newCargoFirm )
        {
            if (newCargoFirm == null)
                return BadRequest();
            else
            {
                using (var dbContext = new DBContext())
                {
                    var exCargoFirm =dbContext.CargoFirm.AsNoTracking().SingleOrDefault(a => a.ID.Equals(newCargoFirm.ID));

                    if (exCargoFirm != null) {
                        exCargoFirm.Name = newCargoFirm.Name;
                        exCargoFirm.PricePerDesi = newCargoFirm.PricePerDesi;
                        dbContext.Entry(exCargoFirm).State  = EntityState.Modified;
                        dbContext.SaveChanges();
                    }
                    return Json(exCargoFirm);
                }
            }
        }
    }


        
 
}

