using System.Collections.Generic;
using System.Linq;
using coreangular.EF;
using coreangular.Model;
using coreangular.Models;
using coreangular.Models.DTO;
using coreangular.Models.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace coreangular.Controllers
{
    public class PackageController : Controller
    {
        [HttpGet]
        public IActionResult GetPackagesById(int id)
        {

            using (var dbContext = new DBContext())
            {
                if (id == 0)
                {
                    var packageList = dbContext.Packages;


                    List<PackageDTO> packList = new List<PackageDTO>();
                    List<Package> packages = packageList.ToList();
                    foreach (Package a in packages)
                    {
                        PackageDTO pck = setPackageDTO(a);

                        packList.Add(pck);
                    }

                    return Json(packList);
                }
                else
                {

                    var packageList = dbContext.Packages.Where(a => a.ID == id);

                    List<PackageDTO> packList = new List<PackageDTO>();
                    List<Package> packages = packageList.ToList();
                    foreach (Package a in packages)
                    {
                        PackageDTO pck = setPackageDTO(a);
                        packList.Add(pck);
                    }

                    return Json(packList);
                }

            }
        }

        public PackageDTO setPackageDTO(Package a)
        {
            using (var dbContext = new DBContext())
            {
                PackageDTO pck = new PackageDTO();
                pck.PackageID = a.ID;
                pck.Price = a.Price;
                var packageowner = dbContext.PackageOwner.AsNoTracking().SingleOrDefault(x => x.ID == a.OwnerID);
                pck.Owner = new OwnerDTO();
                pck.Owner.TC = packageowner.TC;
                pck.Owner.Name = packageowner.Name;
                pck.Owner.Surname = packageowner.Surname;
                pck.Owner.Phone = packageowner.Phone;
                pck.Owner.Email = packageowner.Email;

                pck.FirmID = a.CargoFirmID;
                var firmname = dbContext.CargoFirm.AsNoTracking().SingleOrDefault(x => x.ID == a.CargoFirmID);
                pck.FirmName = firmname.Name;
                pck.Weight = a.Weight;
                pck.Width = a.Width;
                pck.Height = a.Height;
                pck.Depth = a.Depth;
                pck.DueDate = a.DueDate;
                var fromaddressID =
                    dbContext.Address.AsNoTracking()
                        .SingleOrDefault(x => x.PackageId == a.ID && x.Type == AddressType.From);

                var fromAddress = dbContext.Address.AsNoTracking()
                    .SingleOrDefault(x => x.ID == fromaddressID.ID);
                pck.FromAddress = new AddressDTO();
                var cntname =
                    dbContext.Country.AsNoTracking().SingleOrDefault(x => x.ID == fromAddress.CountryID);
                pck.FromAddress.CountryName = cntname.Name;
                var cityname = dbContext.City.AsNoTracking().SingleOrDefault(x => x.ID == fromAddress.CityID);
                pck.FromAddress.CityName = cityname.Name;
                pck.FromAddress.CountryID = fromAddress.CountryID;
                pck.FromAddress.CityID = fromAddress.CityID;
                pck.FromAddress.Distinct = fromAddress.Distinct;
                pck.FromAddress.DistinctSub = fromAddress.DistinctSub;
                pck.FromAddress.Street = fromAddress.Street;
                pck.FromAddress.StreetSub = fromAddress.StreetSub;
                pck.FromAddress.No = fromAddress.No;
                pck.FromAddress.PostCode = fromAddress.PostCode;
                pck.FromAddress.Type = fromAddress.Type;
                pck.FromAddress.Addition = fromAddress.Addition;

                var toaddressID =
                    dbContext.Address.AsNoTracking()
                        .SingleOrDefault(x => x.PackageId == a.ID && x.Type == AddressType.To);

                var toAddress = dbContext.Address.AsNoTracking().SingleOrDefault(x => x.ID == toaddressID.ID);
                pck.ToAddress = new AddressDTO();
                cntname = dbContext.Country.AsNoTracking().SingleOrDefault(x => x.ID == toAddress.CountryID);
                pck.ToAddress.CountryName = cntname.Name;
                cityname = dbContext.City.AsNoTracking().SingleOrDefault(x => x.ID == toAddress.CityID);
                pck.ToAddress.CityName = cityname.Name;
                pck.ToAddress.CountryID = toAddress.CountryID;
                pck.ToAddress.CityID = toAddress.CityID;
                pck.ToAddress.Distinct = toAddress.Distinct;
                pck.ToAddress.DistinctSub = toAddress.DistinctSub;
                pck.ToAddress.Street = toAddress.Street;
                pck.ToAddress.StreetSub = toAddress.StreetSub;
                pck.ToAddress.No = toAddress.No;
                pck.ToAddress.PostCode = toAddress.PostCode;
                pck.ToAddress.Type = toAddress.Type;
                pck.ToAddress.Addition = toAddress.Addition;
                return pck;
            }

        }
        [HttpGet]
        public IActionResult GetOwnerPackage(string id)
        {
            using (var dbContext = new DBContext())
            {

                var owner = dbContext.PackageOwner.Where(a => a.TC.Contains(id));




                List<PackageDTO> packList = new List<PackageDTO>();
                List<Package> packages = new List<Package>();

                foreach (var ow in owner.ToList())
                {
                    var packageList = dbContext.Packages.Where(x => x.OwnerID == ow.ID);
                    foreach (var pac in packageList.ToList())
                    {
                        Package p = new Package();
                        p.ID = pac.ID;
                        p.DueDate = pac.DueDate;
                        p.Weight = pac.Weight;
                        p.Width = pac.Width;
                        p.Price = pac.Price;
                        p.Depth = pac.Depth;
                        p.Height = pac.Height;
                        p.OwnerID = pac.OwnerID;
                        p.CargoFirmID = pac.CargoFirmID;
                        packages.Add(p);
                    }

                }
                foreach (Package a in packages)
                {
                    PackageDTO pck = setPackageDTO(a);
                    packList.Add(pck);
                }

                return Json(packList);
            }

        }
        [HttpGet]
        public IActionResult GetPackages(int id)
        {

            using (var dbContext = new DBContext())
            {
                if (id == 0)
                {
                    var packageList = dbContext.Packages.AsNoTracking().ToList();


                    List<PackageDTO> packList = new List<PackageDTO>();
                    foreach (Package a in packageList)
                    {
                        PackageDTO pck = setPackageDTO(a);
                        packList.Add(pck);
                    }

                    return Json(packList);

                }
                else
                {

                    var packageList = dbContext.Packages.Where(a => a.CargoFirmID == id);


                    List<PackageDTO> packList = new List<PackageDTO>();
                    List<Package> packages = packageList.ToList();
                    foreach (Package a in packages)
                    {
                        PackageDTO pck = setPackageDTO(a);
                        packList.Add(pck);
                    }

                    return Json(packList);
                }

            }
        }

        public bool ValidatePackageModel(PackageDTO packageModel)
        {
            if (packageModel == null)
                return false;
            if (packageModel.Owner.TC == "" || packageModel.Owner.TC.Length!=11|| packageModel.Owner.Email == "" || packageModel.Owner.Phone == "" || packageModel.Owner.Phone.Length !=11|| packageModel.Owner.Name == "" || packageModel.Owner.Surname == "" || packageModel.FirmID == 0 || packageModel.Depth == 0 || packageModel.DueDate == "" || packageModel.FromAddress == null || packageModel.Height == 0 || packageModel.ToAddress == null || packageModel.Weight == 0 || packageModel.Width == 0)
            {
                return false;
            }
            else
                return true;
        }
        [HttpPost]
        public IActionResult CreatePackage([FromBody] PackageDTO packageModel)
        {

            if (!ValidatePackageModel(packageModel))
            {
                var error = new ErrorDTO();
                error.message = "Wrong Model";
                error.statuscode = 400;
                return Json(error);
            }
        
            else
            {
                using (var dbContext = new DBContext())
                {
                   
                  
                    var newOwner = new PackageOwner();
                    newOwner.Email = packageModel.Owner.Email;
                    newOwner.TC = packageModel.Owner.TC;
                    newOwner.Name = packageModel.Owner.Name;
                    newOwner.Surname = packageModel.Owner.Surname;
                    newOwner.Phone = packageModel.Owner.Phone;
                    dbContext.PackageOwner.Add(newOwner);

                    var newPackage = new Package();

                    newPackage.Owner = newOwner;

                    var cargoFirmList = dbContext.CargoFirm.AsNoTracking().SingleOrDefault(a => a.ID.Equals(packageModel.FirmID));


                    newPackage.CargoFirmID = cargoFirmList.ID;

                    newPackage.Depth = packageModel.Depth;
                    newPackage.DueDate = packageModel.DueDate;
                    newPackage.Height = packageModel.Height;
                    newPackage.Weight = packageModel.Weight;
                    newPackage.Width = packageModel.Width;
                    newPackage.Price = packageModel.Price;

                    var retPackage = dbContext.Packages.Add(newPackage);

                    var addressFrom = new Address();
                    addressFrom.CountryID = packageModel.FromAddress.CountryID;
                    addressFrom.Distinct = packageModel.FromAddress.Distinct;
                    addressFrom.DistinctSub = packageModel.FromAddress.DistinctSub;
                    addressFrom.No = packageModel.FromAddress.No;
                    addressFrom.PackageId = retPackage.Entity.ID;
                    addressFrom.PostCode = packageModel.FromAddress.PostCode;
                    addressFrom.Street = packageModel.FromAddress.Street;
                    addressFrom.Type = AddressType.From;
                    addressFrom.Addition = packageModel.FromAddress.Addition;
                    addressFrom.StreetSub = packageModel.FromAddress.StreetSub;
                    addressFrom.CityID = packageModel.FromAddress.CityID;

                    var addressTo = new Address();
                    addressTo.CountryID = packageModel.ToAddress.CountryID;
                    addressTo.Distinct = packageModel.ToAddress.Distinct;
                    addressTo.DistinctSub = packageModel.ToAddress.DistinctSub;
                    addressTo.No = packageModel.ToAddress.No;
                    addressTo.PackageId = retPackage.Entity.ID;
                    addressTo.PostCode = packageModel.ToAddress.PostCode;
                    addressTo.Street = packageModel.ToAddress.Street;
                    addressTo.Type = AddressType.To;
                    addressTo.Addition = packageModel.ToAddress.Addition;
                    addressTo.StreetSub = packageModel.ToAddress.StreetSub;
                    addressTo.CityID = packageModel.ToAddress.CityID;
                    dbContext.Address.Add(addressTo);
                    dbContext.Address.Add(addressFrom);

                    dbContext.SaveChanges();



                    var pack = dbContext.Packages.AsNoTracking().SingleOrDefault(a => a.ID.Equals(retPackage.Entity.ID));

                    return Json(pack);
                }
            }
        }
        /*
        [HttpDelete]
        public IActionResult DeletePackage(int id)
        {
            using (var dbContext = new DBContext())
            {
                var package = dbContext.Packages.AsNoTracking().SingleOrDefault(a => a.ID.Equals(id));
                if (package != null)
                    dbContext.Packages.Remove(package);
                dbContext.SaveChanges();

                return Json(package);
            }
        }*/


    }
}



