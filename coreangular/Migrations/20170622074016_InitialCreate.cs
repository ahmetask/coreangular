﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace coreangular.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CargoFirm",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    PricePerDesi = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CargoFirm", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "City",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_City", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Country",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "PackageOwner",
                columns: table => new
                {
                    ID = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageOwner", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Address",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Addition = table.Column<string>(nullable: true),
                    CityID = table.Column<int>(nullable: false),
                    CountryID = table.Column<int>(nullable: false),
                    Distinct = table.Column<string>(nullable: true),
                    DistinctSub = table.Column<string>(nullable: true),
                    No = table.Column<int>(nullable: false),
                    PostCode = table.Column<int>(nullable: false),
                    Street = table.Column<string>(nullable: true),
                    StreetSub = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Address", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Address_City_CityID",
                        column: x => x.CityID,
                        principalTable: "City",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Address_Country_CountryID",
                        column: x => x.CountryID,
                        principalTable: "Country",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FirmCountry",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CountryID = table.Column<int>(nullable: false),
                    FirmID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FirmCountry", x => x.ID);
                    table.ForeignKey(
                        name: "FK_FirmCountry_Country_CountryID",
                        column: x => x.CountryID,
                        principalTable: "Country",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FirmCountry_CargoFirm_FirmID",
                        column: x => x.FirmID,
                        principalTable: "CargoFirm",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Packages",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CargoFirmID = table.Column<int>(nullable: false),
                    Depth = table.Column<int>(nullable: false),
                    FromAddrID = table.Column<int>(nullable: false),
                    Height = table.Column<int>(nullable: false),
                    OwnerID = table.Column<string>(nullable: true),
                    ToAddrID = table.Column<int>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    Weight = table.Column<int>(nullable: false),
                    Width = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Packages", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Packages_CargoFirm_CargoFirmID",
                        column: x => x.CargoFirmID,
                        principalTable: "CargoFirm",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Packages_Address_FromAddrID",
                        column: x => x.FromAddrID,
                        principalTable: "Address",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Packages_PackageOwner_OwnerID",
                        column: x => x.OwnerID,
                        principalTable: "PackageOwner",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Packages_Address_ToAddrID",
                        column: x => x.ToAddrID,
                        principalTable: "Address",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FirmToCity",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CityID = table.Column<int>(nullable: false),
                    FirmCountryID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FirmToCity", x => x.ID);
                    table.ForeignKey(
                        name: "FK_FirmToCity_City_CityID",
                        column: x => x.CityID,
                        principalTable: "City",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FirmToCity_FirmCountry_FirmCountryID",
                        column: x => x.FirmCountryID,
                        principalTable: "FirmCountry",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Address_CityID",
                table: "Address",
                column: "CityID");

            migrationBuilder.CreateIndex(
                name: "IX_Address_CountryID",
                table: "Address",
                column: "CountryID");

            migrationBuilder.CreateIndex(
                name: "IX_FirmCountry_CountryID",
                table: "FirmCountry",
                column: "CountryID");

            migrationBuilder.CreateIndex(
                name: "IX_FirmCountry_FirmID",
                table: "FirmCountry",
                column: "FirmID");

            migrationBuilder.CreateIndex(
                name: "IX_FirmToCity_CityID",
                table: "FirmToCity",
                column: "CityID");

            migrationBuilder.CreateIndex(
                name: "IX_FirmToCity_FirmCountryID",
                table: "FirmToCity",
                column: "FirmCountryID");

            migrationBuilder.CreateIndex(
                name: "IX_Packages_CargoFirmID",
                table: "Packages",
                column: "CargoFirmID");

            migrationBuilder.CreateIndex(
                name: "IX_Packages_FromAddrID",
                table: "Packages",
                column: "FromAddrID");

            migrationBuilder.CreateIndex(
                name: "IX_Packages_OwnerID",
                table: "Packages",
                column: "OwnerID");

            migrationBuilder.CreateIndex(
                name: "IX_Packages_ToAddrID",
                table: "Packages",
                column: "ToAddrID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FirmToCity");

            migrationBuilder.DropTable(
                name: "Packages");

            migrationBuilder.DropTable(
                name: "FirmCountry");

            migrationBuilder.DropTable(
                name: "Address");

            migrationBuilder.DropTable(
                name: "PackageOwner");

            migrationBuilder.DropTable(
                name: "CargoFirm");

            migrationBuilder.DropTable(
                name: "City");

            migrationBuilder.DropTable(
                name: "Country");
        }
    }
}
