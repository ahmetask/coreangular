﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using coreangular.EF;

namespace coreangular.Migrations
{
    [DbContext(typeof(DBContext))]
    [Migration("20170622074016_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("coreangular.Model.CargoFirm", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<int>("PricePerDesi");

                    b.HasKey("ID");

                    b.ToTable("CargoFirm");
                });

            modelBuilder.Entity("coreangular.Model.City", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("City");
                });

            modelBuilder.Entity("coreangular.Model.Country", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("Country");
                });

            modelBuilder.Entity("coreangular.Model.PackageOwner", b =>
                {
                    b.Property<string>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email");

                    b.Property<string>("Name");

                    b.Property<string>("Phone");

                    b.Property<string>("Surname");

                    b.HasKey("ID");

                    b.ToTable("PackageOwner");
                });

            modelBuilder.Entity("coreangular.Models.Address", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Addition");

                    b.Property<int>("CityID");

                    b.Property<int>("CountryID");

                    b.Property<string>("Distinct");

                    b.Property<string>("DistinctSub");

                    b.Property<int>("No");

                    b.Property<int>("PostCode");

                    b.Property<string>("Street");

                    b.Property<string>("StreetSub");

                    b.HasKey("ID");

                    b.HasIndex("CityID");

                    b.HasIndex("CountryID");

                    b.ToTable("Address");
                });

            modelBuilder.Entity("coreangular.Models.FirmCountry", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CountryID");

                    b.Property<int>("FirmID");

                    b.HasKey("ID");

                    b.HasIndex("CountryID");

                    b.HasIndex("FirmID");

                    b.ToTable("FirmCountry");
                });

            modelBuilder.Entity("coreangular.Models.FirmToCity", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CityID");

                    b.Property<int>("FirmCountryID");

                    b.HasKey("ID");

                    b.HasIndex("CityID");

                    b.HasIndex("FirmCountryID");

                    b.ToTable("FirmToCity");
                });

            modelBuilder.Entity("coreangular.Models.Package", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CargoFirmID");

                    b.Property<int>("Depth");

                    b.Property<int>("FromAddrID");

                    b.Property<int>("Height");

                    b.Property<string>("OwnerID");

                    b.Property<int>("ToAddrID");

                    b.Property<DateTime>("ToDate");

                    b.Property<int>("Weight");

                    b.Property<int>("Width");

                    b.HasKey("ID");

                    b.HasIndex("CargoFirmID");

                    b.HasIndex("FromAddrID");

                    b.HasIndex("OwnerID");

                    b.HasIndex("ToAddrID");

                    b.ToTable("Packages");
                });

            modelBuilder.Entity("coreangular.Models.Address", b =>
                {
                    b.HasOne("coreangular.Model.City", "City")
                        .WithMany()
                        .HasForeignKey("CityID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("coreangular.Model.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("coreangular.Models.FirmCountry", b =>
                {
                    b.HasOne("coreangular.Model.Country", "Country")
                        .WithMany("FirmCountries")
                        .HasForeignKey("CountryID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("coreangular.Model.CargoFirm", "CargoFirm")
                        .WithMany("FirmCountries")
                        .HasForeignKey("FirmID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("coreangular.Models.FirmToCity", b =>
                {
                    b.HasOne("coreangular.Model.City", "City")
                        .WithMany("FirmToCities")
                        .HasForeignKey("CityID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("coreangular.Models.FirmCountry", "FirmCountry")
                        .WithMany("FirmToCities")
                        .HasForeignKey("FirmCountryID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("coreangular.Models.Package", b =>
                {
                    b.HasOne("coreangular.Model.CargoFirm", "CargoFirm")
                        .WithMany("Packages")
                        .HasForeignKey("CargoFirmID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("coreangular.Models.Address", "From")
                        .WithMany("Packages")
                        .HasForeignKey("FromAddrID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("coreangular.Model.PackageOwner", "Owner")
                        .WithMany("Packages")
                        .HasForeignKey("OwnerID");

                    b.HasOne("coreangular.Models.Address", "To")
                        .WithMany("Packages2")
                        .HasForeignKey("ToAddrID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
