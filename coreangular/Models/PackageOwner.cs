﻿using System.Collections.Generic;
using coreangular.Models;

namespace coreangular.Model
{
    public class PackageOwner
    {

        public PackageOwner()
        {
            Package = new List<Package>();
        }
        public  int ID { get; set; }
        public string TC { get; set; }
        
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }

        public string Email { get; set; }

        public   List<Package> Package { get; set; }


    }
}
