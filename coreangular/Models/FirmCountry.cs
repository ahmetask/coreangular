﻿using coreangular.Model;

namespace coreangular.Models
{
    public class FirmCountry
    {

      
        public int ID { get; set; }
      


        public int FirmID { get; set; }
        public CargoFirm CargoFirm { get; set; }


        public int CountryID { get; set; }
        public Country Country { get; set; }


        public  int CityID { get; set; }

        public  City City { get; set; }

       

    }

}
