﻿using System.Collections.Generic;
using coreangular.Models;

namespace coreangular.Model
{
    public class Country

    {
        public Country()
        {
            FirmCountries = new List<FirmCountry>(); 
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public List<FirmCountry> FirmCountries { get; set; }
       
    }
}
