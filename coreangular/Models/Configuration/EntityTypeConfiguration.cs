﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace coreangular.Models.Configuration
{
    public abstract class EntityTypeConfiguration<TEntity> where TEntity : class
    {
        public abstract void Map(EntityTypeBuilder<TEntity> builder);
    }
}
