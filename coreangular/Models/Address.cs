﻿using coreangular.Model;
using coreangular.Models.Enums;


namespace coreangular.Models
{
    public class Address
    {

   
        public int ID { get ; set ; }

        public AddressType Type { get; set; }
        public int CountryID { get; set; }
        public Country Country { get; set; }

        public int CityID { get; set; }
        public City City { get; set; }
        public string Distinct { get; set; }
        public string DistinctSub { get; set; }
        public string Street { get; set; }
        public string StreetSub { get; set; }
        public int No { get; set; }
        public int PostCode { get; set; }
        public string Addition { get; set; }

        public int PackageId { get; set; }
   
        public Package Package { get; set; }




    }
}
