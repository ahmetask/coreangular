﻿using System;
using System.Collections.Generic;
using coreangular.Model;


namespace coreangular.Models
{
    public class Package
    {

        public Package()
        {
            Address = new List<Address>();
        }
        public int ID { get; set; }


      
        public List<Address> Address { get; set; }

   
        public String DueDate { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Depth { get; set; }
        public int Weight { get; set; }
        public int OwnerID { get; set; }
        public PackageOwner Owner { get; set; }
        public int CargoFirmID { get; set; }
        public CargoFirm CargoFirm { get; set; }

        public double Price { get; set; }

       
    }
}
