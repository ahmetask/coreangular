﻿using coreangular.Model;
using coreangular.Models.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace coreangular.Models
{
    public class CountryMap : EntityTypeConfiguration<Country>
    {
        public override void Map(EntityTypeBuilder<Country> builder)
        {
            builder.ToTable("Country");
            builder.HasKey(p => p.ID);
            builder.Property(a => a.Name).IsRequired();

            builder.HasMany(p => p.FirmCountries).WithOne(b => b.Country).HasForeignKey(b => b.CountryID);
        }
    }
}

