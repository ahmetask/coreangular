﻿using coreangular.Models.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace coreangular.Models.Map
{
    public class CityMap : EntityTypeConfiguration<City>
    {
        public override void Map(EntityTypeBuilder<City> builder)
        {
            builder.ToTable("City");
            builder.HasKey(p => p.ID);
            builder.Property(a => a.Name).IsRequired();

            builder.HasMany(p => p.FirmCountries).WithOne(b => b.City).HasForeignKey(b => b.CityID);
        }
    }
}

