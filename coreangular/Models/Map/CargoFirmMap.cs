﻿using coreangular.Model;
using coreangular.Models.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace coreangular.Models
{
    public class CargoFirmMap : EntityTypeConfiguration<CargoFirm>
    {
   

        public override void Map(EntityTypeBuilder<CargoFirm> builder)
        {

            builder.ToTable("CargoFirm");
            builder.HasKey(p => p.ID);

            builder.Property(a => a.PricePerDesi).IsRequired();

            builder.HasMany(a => a.Packages).WithOne(b => b.CargoFirm).HasForeignKey(b=>b.CargoFirmID);

            builder.HasMany(p => p.FirmCountries).WithOne(b=>b.CargoFirm).HasForeignKey(b => b.FirmID);
        }
    }
}
