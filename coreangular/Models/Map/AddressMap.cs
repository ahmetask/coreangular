﻿using coreangular.Models.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace coreangular.Models
{
    public class AddressMap : EntityTypeConfiguration<Address>

    {   
        public override void Map(EntityTypeBuilder<Address> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("Address");
            
            entityTypeBuilder.HasKey(a => a.ID);
            entityTypeBuilder.Property(a => a.CityID).IsRequired();
            entityTypeBuilder.Property(a => a.CountryID).IsRequired();


            entityTypeBuilder.Property(a => a.Distinct).IsRequired();
            entityTypeBuilder.Property(a => a.Street).IsRequired();
     
            entityTypeBuilder.Property(a => a.No).IsRequired();
            entityTypeBuilder.Property(a => a.PostCode).IsRequired();
            entityTypeBuilder.Property(a => a.PackageId).IsRequired();
            entityTypeBuilder.Property(a => a.Type).IsRequired();
            
           

            entityTypeBuilder.HasOne(b => b.Package).WithMany(h => h.Address).HasForeignKey(b => b.PackageId);

        }
    }
}
