﻿using coreangular.Models.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace coreangular.Models.Map
{
    public class PackageMap : EntityTypeConfiguration<Package>

    {
        public override void Map(EntityTypeBuilder<Package> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("Package");

            entityTypeBuilder.HasKey(a => a.ID);

            entityTypeBuilder.Property(a => a.Width).IsRequired();
            entityTypeBuilder.Property(a => a.Height).IsRequired();
            entityTypeBuilder.Property(a => a.Depth).IsRequired();
            entityTypeBuilder.Property(a => a.Weight).IsRequired();

            


        }
    }
}
