﻿using coreangular.Models.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace coreangular.Models.Map
{
    public class FirmCountryMap  : EntityTypeConfiguration<FirmCountry>
    {
        public override void Map(EntityTypeBuilder<FirmCountry> builder)
        {
            builder.ToTable("FirmCountry");

            builder.HasKey(p => p.ID);
          
        }
    }
}
