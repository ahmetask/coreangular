﻿using coreangular.Models.Configuration;
using coreangular.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace coreangular.Models.Map
{
    public class PackageOwnerMap : EntityTypeConfiguration<PackageOwner>
    {
        public override void Map(EntityTypeBuilder<PackageOwner> builder)
        {
            builder.ToTable("PackageOwner");

            builder.HasKey(p => p.ID);
            builder.HasMany(p => p.Package).WithOne(b => b.Owner).HasForeignKey(b=>b.OwnerID).IsRequired();
           

        }
    }
}
