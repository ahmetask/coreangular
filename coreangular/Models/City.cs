﻿using System.Collections.Generic;

namespace coreangular.Models
{
    public class City
    {
        public City()
        {
            FirmCountries = new List<FirmCountry>();
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public List<FirmCountry> FirmCountries { get; set; }
    }
}
