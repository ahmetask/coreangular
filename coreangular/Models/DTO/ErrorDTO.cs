﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace coreangular.Models.DTO
{
    public class ErrorDTO
    {   

        public  int statuscode { get; set; }
        public string message { get; set; }

    }
    
}
