﻿using coreangular.Models.Enums;

namespace coreangular.Models.DTO
{
    public class AddressDTO
    {   

        public  string CountryName { get; set; }
        public int CountryID { get; set; }



        public string CityName { get; set; }
        public int CityID { get; set; }

        public string Distinct { get; set; }
        public string DistinctSub { get; set; }
        public string Street { get; set; }
        public string StreetSub { get; set; }
        public int No { get; set; }
        public int PostCode { get; set; }
        public AddressType Type { get; set; }
        public string Addition { get; set; }
    }
}
