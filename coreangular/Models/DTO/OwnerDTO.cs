﻿namespace coreangular.Models.DTO
{
    public class OwnerDTO
    {   
        public int ID { get; set; }
        public string TC { get; set; } //TC

        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }

        public string Email { get; set; }
    }
}
