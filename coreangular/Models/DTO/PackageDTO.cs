﻿using System;

namespace coreangular.Models.DTO
{
    public class PackageDTO
    {
        public PackageDTO()
        {

        }

        public int PackageID { get; set; }
        public OwnerDTO Owner { get; set; }
        public int FirmID { get; set; }
        public string FirmName { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Depth { get; set; }
        public int Weight { get; set; }
        public String DueDate { get; set; }
        public AddressDTO FromAddress { get; set; }
        public AddressDTO ToAddress { get; set; }

        public double Price { get; set; }
    }
}
