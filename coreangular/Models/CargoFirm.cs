﻿using System.Collections.Generic;
using coreangular.Models;

namespace coreangular.Model
{
    public class CargoFirm
    {
        public CargoFirm()
        {
            FirmCountries = new List<FirmCountry>();
            Packages = new List<Package>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public int PricePerDesi{ get; set; }       
        public ICollection<Package> Packages { get; set; }

        public ICollection<FirmCountry> FirmCountries{ get; set; }

    }
}
