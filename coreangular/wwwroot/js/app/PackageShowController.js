﻿(function () {
    'use strict';

    angular.module("myApp").controller('PackageShowController', PackageShowController);

    PackageShowController.$inject = ['Services'];


    function PackageShowController(Services) {
        var vm = this;

        vm.cargoFirms = [];
        vm.selectedFirmId = 0;
        vm.Packages = [];
        initController();
        vm.OwnerID = null;
        vm.PackID = null;
        vm.orderLimit = 50; 
        vm.OrderBy = "packageID";

        vm.PackageIDFilter = null;
        vm.TCFilter = null; 
        vm.NameFilter = null; 
        vm.SurnameFilter = null; 
        vm.PhoneFilter = null; 
        vm.EmailFilter = null; 
        vm.DueDateFilter = null; 
        vm.WidthFilter = null; 
        vm.HeightFilter = null; 
        vm.WeightFilter = null; 
        vm.DepthFilter = null; 
        vm.PriceFilter = null; 
        vm.FirmNameFilter = null; 
        vm.FromCountryFilter = null; 
        vm.FromCityFilter = null; 
        vm.ToCountryFilter = null; 
        vm.ToCityFilter = null; 

        vm.getPackagesByFirm = getPackagesByFirm;
        vm.getPackagesByOwner = getPackagesByOwner;
        vm.getPackageByPackID = getPackageByPackID;
        vm.sortBy = sortBy;
        function initController() {
            getCargoFirm();
        }

        function getCargoFirm() {
            console.log("getCargoFirm");

            Services.GetCargoFirm()
                .then(function (cargoFirms) {
                    vm.cargoFirms = cargoFirms;
                    console.log(vm.cargoFirms);
                });

        }

        function getPackagesByFirm() {
            console.log("getPackageByFirm");
            Services.GetPackage(vm.selectedFirmId)
                .then(function(packages) {
                    vm.Packages = packages;
                    console.log(vm.Packages);
                });

        }
        function getPackagesByOwner() {
            console.log("getPackagesByOwner");
            Services.GetPackageByOwner(vm.OwnerID)
                .then(function (packages) {
                    vm.Packages = packages;
                    console.log(vm.Packages);
                });

        }
        function getPackageByPackID() {
            console.log("getPackageByPackID");
            Services.GetPackageByID(vm.PackID)
                .then(function (packages) {
                    vm.Packages = packages;
                    console.log(vm.Packages);
                });

        }
        function sortBy(x) {
            console.log("sorting by : ", x);
          
            vm.OrderBy = x;
        }

    }




})();