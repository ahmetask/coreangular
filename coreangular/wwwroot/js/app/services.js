﻿(function () {

    'use strict';

    angular
        .module('myApp')
        .factory('Services', Services);
    Services.$inject = ['$http'];

    function Services($http) {
        var service = {};

        service.GetCargoFirm = GetCargoFirm;
        service.GetCountry = GetCountry;
        service.GetCity = GetCity;
        service.CreatePackage = CreatePackage;
        service.GetPackage = GetPackage;
        service.GetPackageByOwner = GetPackageByOwner;
        service.GetPackageByID = GetPackageByID;

        return service;

        function GetCargoFirm() {

            return $http.get("/CargoFirm/GetCargoFirm").then(handleSuccess, handleError('Error getting all cargo firm'));

        }
        function GetCountry(id) {
            var url = "/CargoFirm/GetCountry/";
            url = url.concat(id.toString());
            return $http.get(url).then(handleSuccess, handleError('Error getting countries'));

        }
        function GetCity(id) {

            var url = "/CargoFirm/GetCities/";
            url = url.concat(id.toString());
            return $http.get(url).then(handleSuccess, handleError('Error getting cities'));

        }
        function CreatePackage(data) {

            return $http.post("/Package/CreatePackage",data).then(handleSuccess, handleError('Error empty field'));

        }
        function GetPackage(id) {
            var url = "/Package/GetPackages/";
            url = url.concat(id.toString());
            return $http.get(url).then(handleSuccess, handleError('Error getting all packages'));

        }
        function GetPackageByOwner(id) {
            var url = "/Package/GetOwnerPackage/";
            url = url.concat(id.toString());
            return $http.get(url).then(handleSuccess, handleError('Error getting all cargo firm'));

        }
        function GetPackageByID(id) {
            var url = "/Package/GetPackagesById/";
            url = url.concat(id.toString());
            return $http.get(url).then(handleSuccess, handleError('Error getting all cargo firm'));

        }
        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }

    }


})();