﻿

(function() {

    var app = angular.module("myApp", ["ngRoute"]);

   
    app.config(function ($routeProvider) {
        $routeProvider
            .when("/create",
            {
                templateUrl: "../../createpackage.html",
                controller: "PackageCreateController",
                controllerAs: "vm"
            })
           .when("/show",
            {
                templateUrl: "../../showpackage.html",
                controller: "PackageShowController",
                controllerAs: "vm"
            });


    });
    
})();