﻿(function () {
    "use strict";

    angular.module("myApp").controller("PackageCreateController", PackageCreateController);

        PackageCreateController.$inject = ["Services"];
 
 
        function PackageCreateController(Services) {
            var vm = this;

            vm.selectedCountryIDFrom = 0;
            vm.selectedcountryIDTo = 0;
            vm.selectedcityIDFrom = 0;
            vm.selectedcityIDTo = 0;
            vm.selectedFirmId = 0;
            vm.date = "";
            vm.price = 0;
            vm.Width= null;
            vm.Height = null;
            vm.Depth = null;
            vm.Weight = null;
            vm.today = 0;
            vm.pricedatefactor = 0;
            vm.packprice = 0;
            vm.tomorrow = 1;
            vm.threedays = 3;
            vm.countries = [];     
            vm.cargoFirms = [];
            vm.citiesFrom = [];
            vm.citiesTo = [];

       

            initController();

            vm.getFirmCountryies = getFirmCountryies;
            vm.getCities = getCities;
            vm.setCityID = setCityID;
            vm.postPackage = postPackage;
            vm.setPrice = setPrice;
          
                    function initController() {
                        console.log("init service");
                        getCargoFirm();
                    }
                    function getCargoFirm() {
                        console.log("getCargoFirm");
                     
                        Services.GetCargoFirm()
                            .then(function (cargoFirms) {
                                vm.cargoFirms = cargoFirms;
                                console.log(vm.cargoFirms);
                            });
                        
                    }
             
                    function getFirmCountryies() {
                        console.log("vm.selectedFirmId:", vm.selectedFirmId);
                        vm.cargoFirms.forEach(function (arrayItem) {
                            if (arrayItem.id === vm.selectedFirmId) {
                                vm.price = arrayItem.pricePerDesi;
                                
                            }
                            
                      
                        });
                        Services.GetCountry(vm.selectedFirmId)
                            .then(function (countries) {
                                vm.countries = countries;                               
                                console.log(vm.countries);
                            });

                    }
                    function getCities(type) {
                        console.log("vm.countries", vm.countries);
                        console.log("vm.selectedCountryIDFrom", vm.selectedCountryIDFrom);
                        console.log("vm.selectedcountryIDTo", vm.selectedcountryIDTo);

                        if (type === 0) {
                            console.log("from:" + vm.selectedCountryIDFrom);
                            Services.GetCity(vm.selectedCountryIDFrom)
                                .then(function (cities) {
                                    vm.citiesFrom = cities;                                
                                    console.log(vm.citiesFrom);
                                });

                        } else {
                          
                            console.log("to:" + vm.selectedcountryIDTo);
                            Services.GetCity(vm.selectedcountryIDTo)
                                .then(function (cities) {
                                    vm.citiesTo = cities;
                                    console.log(vm.citiesTo );
                                });

                        }
                       
                        
                        
                    
                    }
                    function setCityID() {
                        console.log("vm.selectedcityIDFrom ", vm.selectedcityIDFrom);
                        console.log("vm.selectedcityIDTo ", vm.selectedcityIDTo);
                    }
                    function validate() {
                        var ret = true;
                        var idList = ["DistinctF", "DistinctSubF", "StreetF", "StreetSubF", "NoF", "PostCodeF",
                            "AdditionF", "DistinctT", "DistinctSubT", "StreetT", "StreetSubT", "NoT",
                            "PostCodeT", "AdditionT", "Name", "Surname", "TC", "Phone",
                            "Email", "Width", "Height", "Depth", "Weight"];
               
                        for (var i = 0; i < idList.length; i++) {

                                var el = document.getElementById("input" + i);
                                el.classList.remove("error");

                        }

                        
                        for (var i = 0; i < idList.length; i++) {
                            var e = document.getElementById(idList[i]);
                    
                            if (e.value === "" || e.value<=0) {
                                var el = document.getElementById("input"+i);
                                el.classList.add("error");
                                ret = false;
                            }
                               
                        }
                     
                        return ret;


                    }
                    function postPackage() {

                        if (!validate()) {
                            alert("fill all fields");
                            return;
                        }
                           

                        console.log("postPackage");
                        var data = {
                            Owner: {
                                TC: document.getElementById("TC").value,
                                Name: document.getElementById("Name").value,
                                Surname: document.getElementById("Surname").value,
                                Phone: document.getElementById("Phone").value,
                                Email: document.getElementById("Email").value
                            },
                            Width: document.getElementById("Width").value,
                            FirmID: vm.selectedFirmId,
                            Height: document.getElementById("Height").value,
                            Depth: document.getElementById("Depth").value,
                            Weight: document.getElementById("Weight").value,
                            DueDate: vm.date,
                            FromAddress: {
                                CountryID: vm.selectedCountryIDFrom,
                                CityID: vm.selectedcityIDFrom,
                                Distinct: document.getElementById("DistinctF").value,
                                DistinctSub: document.getElementById("DistinctSubF").value,
                                Street: document.getElementById("StreetF").value,
                                StreetSub: document.getElementById("StreetSubF").value,
                                No: document.getElementById("NoF").value,
                                PostCode: document.getElementById("PostCodeF").value,
                                Type: 0,
                                Addition: document.getElementById("AdditionF").value
                            },
                            ToAddress: {
                                CountryID: vm.selectedcountryIDTo,
                                CityID: vm.selectedcityIDTo,
                                Distinct: document.getElementById("DistinctT").value,
                                DistinctSub: document.getElementById("DistinctSubT").value,
                                Street: document.getElementById("StreetT").value,
                                StreetSub: document.getElementById("StreetSubT").value,
                                No: document.getElementById("NoT").value,
                                PostCode: document.getElementById("PostCodeT").value,
                                Type: 1,
                                Addition: document.getElementById("AdditionT").value
                            },
                            Price: vm.packprice
                        };

                     
                        Services.CreatePackage(data)
                            .then(function (res) {
                             
                                console.log(res);
                                alert(JSON.stringify(res));;

                            });
                      
                    }
                    
                    function setPrice() {

                        if (vm.price === 0) {
                            alert("select firm first");
                        }

                        var desi = 0; 

                        desi = (vm.Width * vm.Weight * vm.Depth) / 3000;
                     
                        if (vm.Weight > desi)
                            desi = vm.Weight;

                        var x = 4 - vm.pricedatefactor;
                      
                     

                        var pricenew = desi * vm.price;

                        

                        pricenew = pricenew + x;
          
                        document.getElementById("pricelabel").innerHTML = "$ " +pricenew ;
                        vm.packprice = pricenew;
                        
                        setDate();


                    }
                    function setDate() {
                        var currentDate = new Date();
                        var currentDay = parseInt(currentDate.getDate()) + parseInt(vm.pricedatefactor);
                        vm.date = currentDate;
                        vm.date.setDate(currentDay);
                       
                    }
               

        }
       

  

})();