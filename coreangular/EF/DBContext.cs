﻿using coreangular.Model;
using coreangular.Models;
using Microsoft.EntityFrameworkCore;
using coreangular.Models.Configuration;
using coreangular.Models.Map;


namespace coreangular.EF
{
    public class DBContext : DbContext
    {
  
        public DbSet<Package> Packages { get; set; }
        public DbSet<CargoFirm> CargoFirm { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<FirmCountry> FirmCountry { get; set; }
        public DbSet<PackageOwner> PackageOwner { get; set; }
       

        protected override void OnModelCreating(ModelBuilder  modelBuilder)
        {

         


            modelBuilder.AddConfiguration(new AddressMap());
            modelBuilder.AddConfiguration(new PackageMap() );
                
            modelBuilder.AddConfiguration(new CargoFirmMap());


            modelBuilder.AddConfiguration(new CountryMap());

            modelBuilder.AddConfiguration(new FirmCountryMap());

            modelBuilder.AddConfiguration(new PackageOwnerMap());
            


        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=.\\SqlExpress;Initial Catalog=shipment;Integrated Security=True");
            
        }
            

    }
}
